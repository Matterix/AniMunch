package dk.lundudvikling.animunch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ItemActivity extends AppCompatActivity {


    private TextView itemTitle;
    private TextView itemDescription;
    private Spinner spinnerEatAll;
    private Spinner spinnerEatSome;
    private Spinner spinnerEatNone;
    private String QUERY;
    private DatabaseReference databaseRef;
    private final String DB_TABLE_NAME = "Animal";
    ArrayList<String> spinnerItemEatAll =  new ArrayList<String>();
    ArrayList<String> spinnerItemEatSome =  new ArrayList<String>();
    ArrayList<String> spinnerItemEatNone =  new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        initializeWidgets();

        Bundle extras = getIntent().getExtras();
        QUERY = extras.getString("Animal");

        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.child(DB_TABLE_NAME).getChildren()){
                    Log.i("TESTTAG","VALUE OF SNAPSHOT" + postSnapshot.getKey().toString());

                    //Her man fanger query og søger efter specifikt det dyr man vil.
                    //Måske sende en intent med og så det man søger efter er det man finder.

                    if (postSnapshot.getKey().toString().equals(QUERY)){
                        itemTitle.setText(postSnapshot.getKey().toString());
                        itemDescription.setText(postSnapshot.child("Description").getValue().toString());
                        for(DataSnapshot test : postSnapshot.child("Eat All").getChildren()){
                            spinnerItemEatAll.add(test.getKey().toString());
                        }
                        for(DataSnapshot test : postSnapshot.child("Eat None").getChildren()){
                            spinnerItemEatNone.add(test.getKey().toString());
                        }
                        for(DataSnapshot test : postSnapshot.child("Eat Some").getChildren()){
                            spinnerItemEatSome.add(test.getKey().toString());
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void initializeWidgets(){
        databaseRef = FirebaseDatabase.getInstance().getReference();
        itemTitle = (TextView)findViewById(R.id.item_name_title);
        itemDescription = (TextView)findViewById(R.id.item_description);
        spinnerEatAll = (Spinner)findViewById(R.id.item_eat_all_spinner);
        spinnerEatSome = (Spinner)findViewById(R.id.item_eat_some_spinner);
        spinnerEatNone = (Spinner)findViewById(R.id.item_eat_none_spinner);

        ArrayAdapter<String> adapterEatAll = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerItemEatAll);

        ArrayAdapter<String> adapterEatSome = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerItemEatSome);

        ArrayAdapter<String> adapterEatNone = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerItemEatNone);

        adapterEatAll.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterEatSome.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterEatNone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerEatAll.setAdapter(adapterEatAll);
        spinnerEatSome.setAdapter(adapterEatSome);
        spinnerEatNone.setAdapter(adapterEatNone);
    }
}
