package dk.lundudvikling.animunch;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference databaseRef;
    private ImageView ivCat;
    private ImageView ivDog;
    private ImageView ivRabbit;


    // TODO: 14-07-2017 FIX spinners så de ser ordentlige ud
    // TODO: 18-07-2017 Lowercase navne i database + når man søger så lav strings toLower
    // TODO: 14-07-2017 Populate spinners med kun det dyr man kigger på.
    // TODO: 06-07-2017 Hent det ned lokalt og benyt cloud servicen.
    // TODO: 14-07-2017 Mindre beskrivelse af dyr? Måske skal den ikke være der???
    // TODO: 15-07-2017 Fange spinner item on click event
    // TODO: 18-07-2017 Hvis en ting ikke findes - Send email med navn på objekt så det kan oprettes
    // TODO: 15-07-2017 I DATABASE STÅR DER TRUE VED ALLE = DETTE SKAL VÆRE EN BESKRIVELSE AF HVORFOR DET ER OK!
    // TODO: 15-07-2017 NÅR MAN VÆLGER EN TING I SPINNER SÅ SKAL DET GÅ IND PÅ EN SIDE MED INFO OM PRÆCIS DET VALGTE (INDE PÅ LØG; TRYKKER PÅ HUND; SÅ SKAL JEG SE HVORFOR LØG ER SKIDT FOR HUND)
    // TODO: 15-07-2017 Når man så vælger en ting i spinner skal den sende datasnapshot getvalue videre og vise det frem i sin egen side, det bliver pisse fedt


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        databaseRef = FirebaseDatabase.getInstance().getReference();
        initializeWidgets();

        ivDog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
                intent.putExtra("Animal","Hund");
                startActivity(intent);
            }
        });

        ivCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
                intent.putExtra("Animal","Kat");
                startActivity(intent);
            }
        });

        ivRabbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
                intent.putExtra("Animal","Kanin");
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_field, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_item_search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent i = new Intent(getApplicationContext(), ItemActivity.class);
                i.putExtra("Animal",query);
                startActivity(i);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    public void initializeWidgets(){
        ivDog = (ImageView)findViewById(R.id.suggested_item_iv1);
        ivRabbit = (ImageView)findViewById(R.id.suggested_item_iv2);
        ivCat = (ImageView)findViewById(R.id.suggested_item_iv3);
    }
}
